package com.service;

import javax.servlet.ServletConfig;

import org.apache.cxf.Bus;
import org.apache.cxf.BusFactory;
import org.apache.cxf.frontend.ServerFactoryBean;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.jaxws.JaxWsServerFactoryBean;
import org.apache.cxf.transport.servlet.CXFNonSpringServlet;

  
public class WebServlet extends CXFNonSpringServlet {  
//  private static final String SERVICE_SUFFIX = "";  
    private static final long serialVersionUID = 1L;    
        
    @Override    
    protected void loadBus(ServletConfig servletConfig) {    
        super.loadBus(servletConfig);    
          
        Bus bus = getBus();    
        BusFactory.setDefaultBus(bus);  
        /**
        StudentServiceImpl mystudent = new StudentServiceImpl();//实现类    
         
        ServerFactoryBean serverFactoryBean = new ServerFactoryBean(); //server工厂    
        serverFactoryBean.setServiceClass(StudentServiceImpl.class);// 接口类    
        serverFactoryBean.setAddress("/mystudent"); //服务请求路径    
        serverFactoryBean.setServiceBean(mystudent);    
        serverFactoryBean.create();    
        */
        
        JaxWsServerFactoryBean factory = new JaxWsServerFactoryBean();
        factory.setAddress("/mystudent");
        factory.setServiceClass(StudentServiceImpl.class);
        factory.getInInterceptors().add(new LoggingInInterceptor());  
        factory.getOutInterceptors().add(new LoggingOutInterceptor());
        factory.create();
    }    
}
 
