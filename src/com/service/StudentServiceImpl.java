package com.service;

import java.util.List;

import com.dao.StudentDaoImpl;
import com.entity.Student;



public class StudentServiceImpl implements StudentService{
	private StudentDaoImpl udi = null;
	public StudentServiceImpl(){
		udi=new StudentDaoImpl();//获取UserDaoImpl方法
	}

	public boolean CreateUser(Student student) throws Exception {
		// TODO Auto-generated method stub
		return udi.doCreate(student);
	}

	
	public boolean deleteUser(Student student) throws Exception {
		// TODO Auto-generated method stub
		return udi.dodelete(student);
	}

	
	public boolean updateUser(Student student) throws Exception {
		// TODO Auto-generated method stub
		return udi.doupdate(student);
	}

	
	public List<Student> findAll() throws Exception {
		// TODO Auto-generated method stub
		return udi.findAll();
	}

	
	public Student findAUserByName(String name) throws Exception {
		// TODO Auto-generated method stub
		return udi.findUserByName(name);
	}

	
	public boolean finduserByName(String name) throws Exception {
		// TODO Auto-generated method stub
		Student user = udi.findUserByName(name);
		if(user==null)
		     return false;
		else
			return true;
	}
	@Override  
    public String sayHello(String username) {  
        return username+" : HelloWorld";  
    }  

}
