
package com.service;

import java.util.List;

import javax.jws.WebService;

import com.entity.Student;


@WebService 
public interface StudentService {
    public boolean CreateUser(Student student)throws Exception;//创建一个新用户
    public boolean deleteUser(Student student)throws Exception;//通过用户ID删除一个用户
    public boolean updateUser(Student student)throws Exception;//跟新用户信息
    public List<Student> findAll()throws Exception;//查询所有用户
    public boolean finduserByName(String name)throws Exception;//通过用户ID判断用户是否存在
    public Student findAUserByName(String name)throws Exception;//通过用户ID查找一个用户
    public String sayHello(String username);   
}