package com.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

@SuppressWarnings("deprecation")
public final class HibernateUtil {  
        private static SessionFactory sessionFactory;
        private HibernateUtil(){
        	
        }
        static{
        	Configuration cfg = new Configuration();
        	cfg.configure();
        	sessionFactory = cfg.buildSessionFactory();
        }
        public static Session getSession(){
        	return sessionFactory.openSession();
        }
}