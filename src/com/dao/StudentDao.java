package com.dao;

import java.util.List;

import com.entity.Student;


public interface StudentDao {
	 public boolean doCreate(Student student)throws Exception;//创建一个新用户
	    public boolean dodelete(Student student)throws Exception;//通过用户ID删除一个用户
	    public boolean doupdate(Student student)throws Exception;//跟新用户信息
	    public List<Student> findAll()throws Exception;//查询所有用户
	    public Student findUserByName(String name)throws Exception;//通过用户ID查找一个用户
	    public String sayHello(String username)throws Exception;   
	    

}
