package com.dao;

import java.util.ArrayList;
import java.util.List;



import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.entity.Student;
import com.util.HibernateUtil;


public class StudentDaoImpl implements StudentDao {
	public StudentDaoImpl() {

	}
    //判断是否成功创建用户
	public boolean doCreate(Student student) throws Exception {
		Session session=null;
		try {
			session=HibernateUtil.getSession();
			session.save(student);
			session.beginTransaction().commit();
			return true;
		} catch (Exception e1) {
			e1.printStackTrace();
			return false;
		}
	}
   //判断是否成功删除用户
	public boolean dodelete(Student student) throws Exception {
		Session session=null;
		try {
			session=HibernateUtil.getSession();
			session.delete(student);
			session.beginTransaction().commit();
			return true;
		} catch (Exception e1) {
			e1.printStackTrace();
			return false;
		}
	}
    //判断是否成功跟新用户信息
	public boolean doupdate(Student student) throws Exception {
		Session session=null;
		try {
			session=HibernateUtil.getSession();
			session.update(student);
			session.beginTransaction().commit();
			return true;
		} catch (Exception e1) {
			e1.printStackTrace();
			return false;
		}
	}
	//查找全部用户
	public List<Student> findAll()throws Exception{
		Session session=null;
		List<Student> list=new ArrayList<Student>();
		try {
			 session=HibernateUtil.getSession();
			   Criteria getAllUser=session.createCriteria(Student.class);
			   list=getAllUser.list();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return list;
	}
	//通过用户ID查找一个用户
	public Student findUserByName(String name)throws Exception{
		Session session=null;
		  List<Student> list=null;
		   try
			{
			   session=HibernateUtil.getSession();
			   Criteria c=session.createCriteria(Student.class);
			   c.add(Restrictions.eq("studentno",name));
			   list=c.list();
			}
			catch (Exception e)
			{
			}
			finally
			{
				if(session!=null)
				{
					session.close();
				}
			}
			if (list.size()>0) return list.get(0);
			else return null;
	}
	@Override  
    public String sayHello(String username) {  
        return username+" : HelloWorld";  
    }  

}
